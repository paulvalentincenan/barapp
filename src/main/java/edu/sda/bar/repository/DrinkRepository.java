package edu.sda.bar.repository;

import edu.sda.bar.entity.Drink;
import org.springframework.data.repository.CrudRepository;

public interface DrinkRepository extends CrudRepository<Drink, Integer > {
}
