package edu.sda.bar.repository;

import edu.sda.bar.entity.Comanda;
import org.springframework.data.repository.CrudRepository;

public interface ComandaRepository extends CrudRepository<Comanda,Integer> {
}
