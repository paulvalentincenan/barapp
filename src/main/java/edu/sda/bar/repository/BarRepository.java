package edu.sda.bar.repository;


import edu.sda.bar.entity.Bar;
import org.springframework.data.repository.CrudRepository;

public interface BarRepository extends CrudRepository <Bar,Integer>{


}
