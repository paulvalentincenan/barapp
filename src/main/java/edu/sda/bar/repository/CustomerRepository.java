package edu.sda.bar.repository;

import edu.sda.bar.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository  extends CrudRepository<Customer,Integer> {

}
