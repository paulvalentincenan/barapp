package edu.sda.bar.repository;

import edu.sda.bar.entity.Masa;
import org.springframework.data.repository.CrudRepository;

public interface MasaRepository extends CrudRepository<Masa,Integer > {
}
