package edu.sda.bar.repository;

import edu.sda.bar.entity.Admin;

import edu.sda.bar.entity.Bar;
import org.springframework.data.repository.CrudRepository;

public interface AdminRepository extends CrudRepository<Admin, Integer>  {
}
