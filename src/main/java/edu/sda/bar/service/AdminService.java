package edu.sda.bar.service;

import edu.sda.bar.entity.*;
import edu.sda.bar.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    @Autowired
    AdminRepository adminRepository;

    public Admin addAdmin(Admin admin) {
        return adminRepository.save(admin);

    }
    @Autowired
    MenuRepository menuRepository;
    public Menu addMenu(Menu menu){
        return menuRepository.save(menu);
    }

    @Autowired
    BarRepository barRepository;
    public Bar addBar(Bar bar) {
            return barRepository.save(bar);

    }
    @Autowired
    MasaRepository masaRepository;
    public Masa addMasa(Masa masa){

        return masaRepository.save(masa);
    }
    @Autowired
    DrinkRepository drinkRepository;
    public Drink addDrink(Drink drink){
        return drinkRepository.save(drink);
    }






}
