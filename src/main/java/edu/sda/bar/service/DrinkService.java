package edu.sda.bar.service;

import edu.sda.bar.entity.Drink;
import edu.sda.bar.repository.DrinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DrinkService {
    @Autowired
    DrinkRepository drinkRepository;

    public Drink insertDrink(Drink drink){
        return drinkRepository.save(drink);
    }

    public Iterable<Drink> allDrinks()
    {
        return drinkRepository.findAll();
    }


    public void deleteDrinkById(Integer id) {
        drinkRepository.deleteById(id);

    }
    }