package edu.sda.bar.service;

import edu.sda.bar.entity.Comanda;
import edu.sda.bar.entity.Customer;
import edu.sda.bar.repository.ComandaRepository;
import edu.sda.bar.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComandaService {


    @Autowired
    ComandaRepository comandaRepository;

    public Comanda insertComanda(Comanda comanda) {
        return comandaRepository.save(comanda);
    }

    public Iterable<Comanda> allcomanda() {
        return comandaRepository.findAll();
    }

    public void deleteComandaById(Integer id) {
        comandaRepository.deleteById(id);

    }
}
