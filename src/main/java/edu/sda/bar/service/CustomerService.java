package edu.sda.bar.service;

import edu.sda.bar.entity.Comanda;
import edu.sda.bar.entity.Customer;
import edu.sda.bar.entity.Masa;
import edu.sda.bar.repository.ComandaRepository;
import edu.sda.bar.repository.CustomerRepository;
import edu.sda.bar.repository.MasaRepository;
import edu.sda.bar.restController.CustomerRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CustomerService {


        @Autowired
        CustomerRepository customerRepository;

        public Customer addCustomer(Customer customer){
            return customerRepository.save(customer);
        }
        @Autowired
    ComandaRepository comandaRepository;
        public Comanda faComanda(Comanda comanda){
            return comandaRepository.save(comanda);
        }
        public Comanda platesteComanda(Comanda comanda){
            return comandaRepository.save(comanda);
        }
        @Autowired
        MasaRepository masaRepository;
        public Masa bookMasa(Masa masa){
            return masaRepository.save(masa);
        }
        public Masa ocupaMasa(Masa masa) {
            return masaRepository.save(masa);
        }
         public Masa platesteMasa(Masa masa){
        return masaRepository.save(masa);
    }

    public ComandaRepository getComandaRepository() {
        return comandaRepository;
    }

    public Iterable<Customer> allCustomers(){
            return customerRepository.findAll();
        }

        public void deleteCustomerById(Integer id) {
             customerRepository.deleteById(id);
        }
//        public Customer updateCustomer(Integer id, Customer customer) {
//            return customerRepository.save();
// }
}




