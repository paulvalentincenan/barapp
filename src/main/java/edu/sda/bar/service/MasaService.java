package edu.sda.bar.service;
import edu.sda.bar.entity.Masa;
import edu.sda.bar.repository.MasaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasaService {
    @Autowired
    MasaRepository masaRepository;

    public Masa insertMasa(Masa masa){
        return  masaRepository.save(masa);
    }

    public Iterable<Masa> allMasa()
    {
        return masaRepository.findAll();
    }


    public void deleteMasaById(Integer id) {
        masaRepository.deleteById(id);

    }
}