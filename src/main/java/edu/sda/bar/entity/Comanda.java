package edu.sda.bar.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Comanda {
    @Id
    @Getter
    @GeneratedValue
    private Integer id;
    @Getter@Setter
    private Integer timeToMake;

}
