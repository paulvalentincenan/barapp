package edu.sda.bar.entity;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Bar {

        @Id
        @GeneratedValue
        @Setter
        private Integer id;
        @Getter@Setter
        private String name;
        @Setter@Getter
        private String address;
        @Getter@Setter
        private Date workingHours;

}
