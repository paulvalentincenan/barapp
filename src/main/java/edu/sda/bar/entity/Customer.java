package edu.sda.bar.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Customer {
    @Id
    @GeneratedValue
    @Getter
    private Integer id;
    @Getter@Setter
    private String name;
    @Getter@Setter
    private Integer barTable;
    @Getter@Setter
    private String phNumber;
    @Getter@Setter
    private String barOrder;




}
