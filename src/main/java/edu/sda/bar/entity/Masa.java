package edu.sda.bar.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Masa {

    @Id
    @GeneratedValue
    @Getter
    private Integer id;
    @Setter@Getter
    private boolean available;
    @Setter@Getter
    private String tableOrder;
}
