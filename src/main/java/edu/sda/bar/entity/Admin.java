package edu.sda.bar.entity;

import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Admin {

    @Id
    @GeneratedValue
    @Setter
    private Integer id;
    @Setter
    private String userName;
    @Setter
    private String password;

}
