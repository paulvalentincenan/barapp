package edu.sda.bar.entity;


import lombok.Getter;
import lombok.Setter;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Drink {
    @Id
    @GeneratedValue
    @Getter
    private Integer id;
    @Getter@Setter
    private String name;
    @Setter@Getter
    private Integer price;
    @Getter@Setter
    private Integer quantity;
    @Getter@Setter
    private Integer timeToMake;
    @Getter@Setter
    private String spirit;
    @Getter@Setter
    private String type;


}
