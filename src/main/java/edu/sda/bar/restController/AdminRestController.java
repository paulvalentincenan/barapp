package edu.sda.bar.restController;


import edu.sda.bar.entity.*;
import edu.sda.bar.service.AdminService;
import edu.sda.bar.service.BarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminRestController {

    @Autowired
    AdminService adminService;

    @PostMapping(path = "/addAdmin")
    public @ResponseBody
    Admin addAdmin(@RequestBody Admin admin) {
        return adminService.addAdmin(admin);
    }

    @PostMapping(path = "/addMenu")
    public @ResponseBody
    Menu addMenu(@RequestBody Menu menu) {
        return adminService.addMenu(menu);

    }

    @PostMapping(path = "/addBar")
    public @ResponseBody
    Bar addBar(@RequestBody Bar bar) {
        return adminService.addBar(bar);
    }

    @PostMapping(path = "/addMasa")
    public @ResponseBody
    Masa addMasa(@RequestBody Masa masa) {
        return adminService.addMasa(masa);

    }

    @PostMapping(path = "/addDrink")
    public @ResponseBody
    Drink addDrink(@RequestBody Drink drink) {
        return adminService.addDrink(drink);
    }
}
