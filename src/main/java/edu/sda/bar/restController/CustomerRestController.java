package edu.sda.bar.restController;

import edu.sda.bar.entity.Comanda;
import edu.sda.bar.entity.Customer;
import edu.sda.bar.entity.Masa;
import edu.sda.bar.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerRestController {

    @Autowired
    CustomerService customerService;


    @PostMapping(path = "/addCustomer")
    public @ResponseBody
    Customer addCustomer(@RequestBody Customer customer) {
        return customerService.addCustomer(customer);
    }

    @PostMapping(path = "/bookMasa")
    public @ResponseBody
    Masa bookMasa(@RequestBody Masa masa) {
        return customerService.bookMasa(masa);
    }

    @PostMapping(path = "/ocupaMasa")
    public @ResponseBody
    Masa ocupaMasa(@RequestBody Masa masa) {
        return customerService.ocupaMasa(masa);
    }

    @PostMapping(path = "/platesteMasa")
    public @ResponseBody
    Masa platesteMasa(@RequestBody Masa masa) {
        return customerService.platesteMasa(masa);
    }

    @PostMapping(path = "/faComanda")
    public @ResponseBody
    Comanda faComanda(@RequestBody Comanda comanda) {
        return customerService.faComanda(comanda);
    }

    @PostMapping(path = "/platesteComanda")
    public @ResponseBody
    Comanda platesteComanda(@RequestBody Comanda comanda) {
        return customerService.platesteComanda(comanda);
    }

//
//        @GetMapping(path = "/getAllCustomers")
//        public @ResponseBody
//        Iterable<Customer> getAllCustomers() {
//            return customerService.allCustomers();
//
//
//        }
//        @DeleteMapping("/deleteCustomer/{id}")
//        public void deleteCustomerById(@PathVariable Integer id) {
//        customerService.deleteCustomerById(id);
//
//        }
//

//        @PutMapping("/updateUser")
//        public  Customer updateCustomer(@RequestParam Integer id, @RequestBody Customer customer) {
//        return customerService.updateCustomer(id, customer);
//    }
}




